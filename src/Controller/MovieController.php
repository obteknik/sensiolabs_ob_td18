<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class MovieController extends AbstractController
{

    /**
     * Base des films
     */
    private static function getFilms()
    {

        return $movies =[
            [
                'id' => 1,
                'title' => 'Titanic',
                'dateAt' => '1998-07-01',
                'director' => 'James Cameron',
                'actors' => 'Leobardo DiCaprio, Kate Winslet, Billy Zane'
            ],
            [
                'id' => 2,
                'title' => 'Pirates des Caraibes : La fontaine de Jouvence',
                'dateAt' => '2011-05-18',
                'director' => 'Rob Marshall',
                'actors' => 'Johnny Depp, Penelop Cruz, Geoffrey Rush'
            ],
            [
                'id' => 3,
                'title' => 'TopGun',
                'dateAt' => '1986-09-17',
                'director' => 'Tony Scott',
                'actors' => 'Tom Cruise, Kelly McGillis, Tom Skerrit'
            ],
            [
                'id' => 4,
                'title' => 'Il était une fois en Amériques',
                'dateAt' => '1984-05-23',
                'director' => 'Sergio Leone',
                'actors' => 'Chuck Low, Robert De Niro, James Woods'
            ],
            [
                'id' => 5,
                'title' => 'A star is born',
                'dateAt' => '2018-03-18',
                'director' => 'Bradley Cooper',
                'actors' => 'Lady Gaga, Bradley Cooper, Sam Elliot'
            ],
            [
                'id' => 6,
                'title' => 'Mia et le lion blanc',
                'dateAt' => '2018-12-26',
                'director' => 'Gilles de Maistre',
                'actors' => 'Daniah De Villiers, Mélanie Laurent, Langley Kirkwood'
            ],
            [
                'id' => 7,
                'title' => 'Aquaman',
                'dateAt' => '2018-12-19',
                'director' => 'James Wan',
                'actors' => 'Jason Momoa, Amber Heard, Willem Dafoe'
            ],
            [
                'id' => 8,
                'title' => 'Creed II',
                'dateAt' => '2019-01-09',
                'director' => 'Steven Caple Jr',
                'actors' => 'Michael B. Jordan, Sylvester Stallone, Tessa Thompson'
            ],
            [
                'id' => 9,
                'title' => 'La mule',
                'dateAt' => '2019-01-23',
                'director' => 'Clint Eastwood',
                'actors' => 'Clint Eastwood, Bradley Cooper, Laurence Fishburne'
                ]
        ];
            
        }
    

    /**
     * Liste des films
     * @Route("/movies", name="movies")
     */
    public function list()
    {

        $movies = self::getFilms();

        return $this->render('movie/index.html.twig', [
            'movies' => $movies,
        ]);
    }

    /**
     * Détail d'un film
     * @Route("/movies/view/{id}", name="movie_view", requirements={"id"="\d+"})
     */
    public function view($id)
    {

        //Films
        $movies = self::getFilms();

        //Variables qui seront passées à la vue
        $title = '';
        $dateAt = '';
        $director= '';
        $actors = '';

        //Récupération des informations du film
        $count = count($movies);   
        
        //Tableau Php des identifiants de film
        $array_id = [];
        for($i=0; $i<$count;$i++){
            $array_id[] = $movies[$i]['id'];
        }

        //je teste si l'identifiant demandé correspond à un identifiant de la base de films, sinon exception
        if(in_array($id,$array_id)){
         for($i=0; $i<$count;$i++){
                if($movies[$i]['id'] == $id){
                    $title = $movies[$i]['title'];
                    $dateAt = $movies[$i]['dateAt'];
                    $director = $movies[$i]['director'];
                    $actors = $movies[$i]['actors'];
                };
            } 
        } else {
            throw $this->createNotFoundException("Le film n'existe pas !!!");
        }

        return $this->render('movie/view.html.twig', [
            'array_id' =>$array_id,
            'title' => $title,
            'dateAt' => $dateAt,
            'director' => $director,
            'actors' => $actors
        ]);
    }
}
